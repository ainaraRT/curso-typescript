# Curso Typescript

Curso sobre cómo utilizar Typescript en [YouTube](https://www.youtube.com/watch?v=Xxqh0RoWxNc&ab_channel=Fazt)


## Instalar Typescript
```shell
PS C:\Users\cursoTypescript> npm i typescript -g

changed 1 package, and audited 2 packages in 4s

found 0 vulnerabilities
```

### Compilación del código
```shell
PS C:\Users\cursoTypescript> tsc salida.ts
```

### Compilación del código automático
```shell
PS C:\Users\cursoTypescript> tsc salida.ts -w
```

## Código del proyecto
[GitHub FaztTech](https://github.com/FaztWeb/typescript-course/tree/master/basics)