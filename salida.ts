console.log('Hello world');

//types
var myString: string = "hello world";
myString = 500 + "";

var myNumber: number = 22;
var myBool: boolean = true || false;

var myVar: any = "hello";
myVar = 22;
myVar = "hello";
myVar = [];
myVar = true;

//por pantalla
document.write(myNumber.toString())
document.write("<br/>" + myString)

//arrays
var AnyArray: any[] = ["", "", ""];
AnyArray = [true, 2, "3"]

var StringArray: string[] = ["item1", "item2", ""];
var NumberArray: number[] = [1, 2, 3];
var BooleanArray: boolean[] = [true, false, true]

//tuple
var strNumTuple: [string, number];
strNumTuple = ["Hello", 1];
//strNumTuple = [1, "Hello"]; //se tendría que convertir
// strNumTuple = ["Hello", 1, "World", 2];

//void, undefined, null
var myVoid: void = undefined;
var myNull: null = undefined || null;
var myUndefined: undefined = undefined;

document.write(typeof("<br>" + strNumTuple));
document.write(typeof("<br>" + myVoid));

//functions
function getSum (num1: number, num2: number): number {
    return num1 + num2;
}

let mySum = function(num1: number | string, num2: number | string): number {
    
    if(typeof(num1) === 'string') {
        num1 = parseInt(num1);
    }

    if(typeof(num2) === 'string') {
        num2 = parseInt(num2);
    }

    return num1 + num2;
}

function getName(firstName: string, lastName?: string): string {
    // return firstName + ' ' + lastName;
    if(lastName == undefined) {
        return firstName;
    }
    return `${firstName} ${lastName}`;
}

document.write("<br>" + getName("Jesús"))
getName("Jesús");

document.write("<br>" + getName("Isaac", "Asimov"))

function myVoidFunction(): void {
    return;
}

//interfaces
interface ITodo {
    title: string;
    text: string;
}

function showToDo(todo: ITodo /*{title: string; text: string}*/) {
    console.log(`${todo.title} - ${todo.text}`)
}

showToDo({
    title: 'Eat Dinner',
    text: 'lorem'
})

let myToDo: ITodo = {title: 'Eat dinner', text: 'lorem'}

//clases
class User {
    private name: string;
    public email: string;
    protected age: number;

    constructor(name: string, email: string, age: number) {
        this.name = name;
        this.email = email;
        this.age = age
    }

    register(){
        console.log(`${this.name} is registered!`)
    }

    showMeAge() {
        return this.age
    }

    public /*private*/ ageInYears() {
        return this.age + ' years';
    }

    payInvoice() {
        console.log(`${this.name} paid invoice`)
    }
}

var john = new User('John', 'john@email.com', 24);
// document.write("<br>" + john.name);

console.log(john.register())
document.write("<br>" + john.register());
document.write("<br>" + john.email);
document.write("<br>" + john.ageInYears());

//herencia
class Member extends User {
    id: number;

    constructor(id, name, email, age) {
        super(name, email, age); //lo hereda de la clase padre
        this.id = id
    }

    payInvoice() {
        super.payInvoice();
    }
}

var gordon = new Member(1, 'Gordon', 'gordon@email.com', 24);
gordon.payInvoice()

document.write("<br>" + gordon.payInvoice());
