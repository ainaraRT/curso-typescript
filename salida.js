var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
console.log('Hello world');
//types
var myString = "hello world";
myString = 500 + "";
var myNumber = 22;
var myBool = true || false;
var myVar = "hello";
myVar = 22;
myVar = "hello";
myVar = [];
myVar = true;
//por pantalla
document.write(myNumber.toString());
document.write("<br/>" + myString);
//arrays
var AnyArray = ["", "", ""];
AnyArray = [true, 2, "3"];
var StringArray = ["item1", "item2", ""];
var NumberArray = [1, 2, 3];
var BooleanArray = [true, false, true];
//tuple
var strNumTuple;
strNumTuple = ["Hello", 1];
//strNumTuple = [1, "Hello"]; //se tendría que convertir
// strNumTuple = ["Hello", 1, "World", 2];
//void, undefined, null
var myVoid = undefined;
var myNull = undefined || null;
var myUndefined = undefined;
document.write(typeof ("<br>" + strNumTuple));
document.write(typeof ("<br>" + myVoid));
//functions
function getSum(num1, num2) {
    return num1 + num2;
}
var mySum = function (num1, num2) {
    if (typeof (num1) === 'string') {
        num1 = parseInt(num1);
    }
    if (typeof (num2) === 'string') {
        num2 = parseInt(num2);
    }
    return num1 + num2;
};
function getName(firstName, lastName) {
    // return firstName + ' ' + lastName;
    if (lastName == undefined) {
        return firstName;
    }
    return "".concat(firstName, " ").concat(lastName);
}
document.write("<br>" + getName("Jesús"));
getName("Jesús");
document.write("<br>" + getName("Isaac", "Asimov"));
function myVoidFunction() {
    return;
}
function showToDo(todo /*{title: string; text: string}*/) {
    console.log("".concat(todo.title, " - ").concat(todo.text));
}
showToDo({
    title: 'Eat Dinner',
    text: 'lorem'
});
var myToDo = { title: 'Eat dinner', text: 'lorem' };
//clases
var User = /** @class */ (function () {
    function User(name, email, age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }
    User.prototype.register = function () {
        console.log("".concat(this.name, " is registered!"));
    };
    User.prototype.showMeAge = function () {
        return this.age;
    };
    User.prototype.ageInYears = function () {
        return this.age + ' years';
    };
    User.prototype.payInvoice = function () {
        console.log("".concat(this.name, " paid invoice"));
    };
    return User;
}());
var john = new User('John', 'john@email.com', 24);
// document.write("<br>" + john.name);
console.log(john.register());
document.write("<br>" + john.register());
document.write("<br>" + john.email);
document.write("<br>" + john.ageInYears());
//herencia
var Member = /** @class */ (function (_super) {
    __extends(Member, _super);
    function Member(id, name, email, age) {
        var _this = _super.call(this, name, email, age) || this;
        _this.id = id;
        return _this;
    }
    Member.prototype.payInvoice = function () {
        _super.prototype.payInvoice.call(this);
    };
    return Member;
}(User));
var gordon = new Member(1, 'Gordon', 'gordon@email.com', 24);
gordon.payInvoice();
document.write("<br>" + gordon.payInvoice());
